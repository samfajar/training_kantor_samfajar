<!DOCTYPE html>
<html>
<head>
<?php $this->load->view('Articles/includes/head'); ?>
</head>
<body>
  <div id="container">
    <div id="out-wraper">
      <?php $this->load->view('Articles/includes/headers'); ?>
      <div id="nav-container"> 
        <?php $this->load->view('Articles/includes/navigation'); ?>
      </div>
      <div class="clear"></div>     
      <div id="content">
        <?php $this->load->view('Articles/includes/content_articles'); ?>
        <?php $this->load->view('Articles/includes/content_sidebar'); ?>
      <div class="clear"></div>  
        <?php $this->load->view('Articles/includes/footer'); ?>
      </div>
    </div>
  </div>           
</body>
</html>
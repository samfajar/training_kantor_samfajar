<!DOCTYPE html>
<html>
<head>
<?php $this->load->view('Articles/includes/head'); ?>
</head>
<body>
  <div id="container">
    <div id="out-wraper">
      <?php $this->load->view('Articles/includes/headers'); ?> 
      <div id="nav-container"> 
        <?php $this->load->view('Articles/includes/navigation'); ?>
      </div>
      <div class="clear"></div>     
      <div id="content">
        <div id="article-container">
          <article> 
            <div class="title-article"> 
              <h3>Edit Article</h3>
            </div>
            <?php echo form_open_multipart('Articles/edit');  ?> 
            <?php echo validation_errors('<p class="error">'); ?>
            <?php foreach ($data as $article ){ ?>
            <input type="hidden" value="<?php echo $article->idarticle;  ?>" name="id">
            <label>Judul</label><input type="text" value="<?php echo $article->judul;?>" name="judul">
            <label>Isi</label><textarea id="textarea" name="isi"><?php echo $article->isi;  ?>
            </textarea>
			
            <div class="footer-article">
              <input type="submit" value="Simpan" class="continue-reading">
              <?php }?>
              <?php echo form_close();  ?>
              <span class="writer">writen by : <b>Walden</b></span>
              <div class="clear"></div>
            </div>
          </article>
        </div>
        <?php $this->load->view('Articles/includes/content_sidebar'); ?>
	<div class="clear"></div>  
        <?php $this->load->view('Articles/includes/footer'); ?>
      </div>
    </div>
  </div>           
</body>
</html>
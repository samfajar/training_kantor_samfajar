<!DOCTYPE html>
<html>
<head>
<?php $this->load->view('Articles/includes/head'); ?>
</head>
<body>
  <div id="container">
    <div id="out-wraper">
      <?php $this->load->view('Articles/includes/headers'); ?> 
      <div id="nav-container"> 
        <?php $this->load->view('Articles/includes/navigation'); ?>
      </div>
      <div class="clear"></div>     
      <div id="content">
        <div id="article-container">
        <?php foreach($data as $content){ ?>
          <article> 
            <div class="title-article"> 
              <div class="date">
                <span class="years">2013</span>
                <span class="day-moon">19</span>
                <span class="day-moon">mar</span>
                <div class="clear"></div>
              </div>
              <h3><?php echo $content->judul ?></h3>
              <span class="no-article">12</span>
            </div>
            <p><?php echo $content->isi ?></p>
            <div class="footer-article">
              <a href="#" class="continue-reading"><span></span>Continue Reading</a>
              <span class="writer">writen by : <b>Walden</b></span>
              <span class="tags">Tags : <a href="#">JAVASCRIPT</a> <a href="<?php echo base_url();?>Articles/delete/<?php echo $content->idarticle;?>">Delete</a> <a href="<?php echo base_url();?>Articles/edit/<?php echo $content->idarticle;?>">Edit</a></span>
              <div class="clear"></div>
            </div>
          </article>
          <?php } ?>
          <?php echo $halaman ?>
          <div class="clear"></div>  
        </div>
      <?php $this->load->view('Articles/includes/content_sidebar'); ?>
      <div class="clear"></div>  
      <?php $this->load->view('Articles/includes/footer'); ?>
      </div>
    </div>
  </div>           
</body>
</html>
      <div id="footer">
        <div class="box">
          <div class="sub-box">
            <h4 class="title-box">About Site</h4>
            <p>'one' (compare 'on', in Saxon dialects) and survi Scots as the number 'ane'. Both 'on' (respelled 'one' by the Normans) and 'an' survived into Modern English, with 'one' used as the number and 'an' ('a', before nouns that begin with a consonant sound) as an indefinite article.</p>
          </div>
        </div>
          <div class="box"> 
            <div class="sub-box">
              <h4 class="title-box">Recent Comments</h4>
              <div class="recent-comment">
                <a href="#">'one' (compare 'on', in Saxon dialects) and survi Scots as the number.....</a>
                <span><b>Comment Author</b>, 10-03-2013</span>
              </div>
              <div class="recent-comment">
                <a href="#">'one' (compare 'on', in Saxon dialects) and survi Scots as the number.....</a>
                <span><b>Comment Author</b>, 10-03-2013</span>
              </div>
              <div class="recent-comment">
                <a href="#">'one' (compare 'on', in Saxon dialects) and survi Scots as the number.....</a>
                <span><b>Comment Author</b>, 10-03-2013</span>
              </div>
              <div class="recent-comment">
                <a href="#">'one' (compare 'on', in Saxon dialects) and survi Scots as the number.....</a>
                <span><b>Comment Author</b>, 10-03-2013</span>
              </div>
            </div>  
          </div>
          <div class="box">
            <div class="sub-box">
              <h4 class="title-box">Popular Post</h4>
              <ul>
                <li><a href="#">Marcury fre Baklit dispkay</a></li>
                <li><a href="#">PT.Walden Global Service</a></li>
                <li><a href="#">survived into Modern English</a></li>
                <li><a href="#">PointBlank</a></li>
              </ul>
            </div>  
          </div>
        <div class="clear"></div>
        <h5>Copy @ 2013 <b>PT.Walden Global Service</b> All Right Reserverd</h5>
        </div>
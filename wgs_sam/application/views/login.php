﻿<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="ie6 ielt8"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="ie7 ielt8"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="ie8"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--> <html lang="en"> <!--<![endif]-->
<head>
<meta charset="utf-8">
<title>Paper Stack</title>
<link rel="stylesheet" type="text/css" href="<?Php echo base_url(); ?>css/style.css" />
</head>
<body>
<div class="container">
	<section id="content">
<?php echo form_open_multipart('user/login');  ?>
			<h1>Login Form</h1>
			<div>
				<input type="text" placeholder="Username" name="username" required="" id="username" />
			</div>
			<div>
				<input type="password" placeholder="Password" name"password" required="" id="password" />
			</div>
			<div>
				<input type="submit" value="Log in" />
				<a href="#">Lost your password?</a>
				<a href="<?php echo base_url();?>/index.php/home/register">Register</a>
			</div>
           <?php echo form_close();  ?>
		<div class="button">
			<a href="">Download source file</a>
		</div><!-- button -->
	</section><!-- content -->
</div><!-- container -->
</body>
</html>

<!DOCTYPE html>
<html>
<head>
<meta name="description" content="Exercise_3">
<meta name="keywords" content="HTML,CSS,Javascript">
<meta name="author" content="Sam Fajar Muharram">
<meta charset="UTF-8">
<title>Exercise_3</title>
        
<link href="<?php echo base_url(); ?>css/Style_home.css" rel="stylesheet" type="text/css">
</head>
<body>
  <div id="container">
    <div id="out-wraper">
      <div id="header">
        <h1>Walden Global Service</h1>
          <br>
          <span>This the exercise 3 whit using html 5 and css 3</span>                        
      </div> 
      <div id="nav-container"> 
        <div id="navi-out">  
          <ul id="navmenu">
            <li><a href="<?php echo base_url();?>">Home</a></li>
            <li><a href="<?php echo base_url();?>index.php/article/add">Add Article</a></li>
            <li><a href="#">Tutor</a>
              <ul id="drop">
                <li><a href="#">HTML</a></li>
                <li><a href="#">CSS</a></li>
                <li><a href="#">Java Script</a></li>
              </ul>
            </li>
            <li><a href="<?php echo base_url();?>index.php/articlel/daftar">List Article</a></li>
            <li><a href="<?php echo base_url();?>index.php/user/logout">Logout</a></li>
          </ul>
        </div>
      </div>
      <div class="clear"></div>     
      <div id="content">
	    <div id="article-container">
          <article> 
            <div class="title-article"> 
             
              <h3>Edit Article</h3>
            </div>
            <?php echo form_open_multipart('article/edit');  ?> 
            <?php echo validation_errors('<p class="error">'); ?>
            <?php foreach ($data as $article ){ ?>
            <input type="hidden" value="<?php echo $article->idarticle;  ?>" name="id">
			<label>Judul</label><input type="text" value="<?php echo $article->judul;?>" name="judul">
            <label>Isi</label><textarea id="textarea" name="isi"><?php echo $article->isi;  ?>
            </textarea>
			
            <div class="footer-article">
              <input type="submit" value="Simpan" class="continue-reading">
              <?php }?>
              <?php echo form_close();  ?>
              <span class="writer">writen by : <b>Walden</b></span>
              <div class="clear"></div>
            </div>
          </article>
          
        </div>
		<div id="sidebar-container">
		  <div class="box">
            <div class="sub-box">
              <h4 class="title-box">Stay in Touch</h4>          
              <div class="social">
                <span>7654</span>
                <a href="#">Twitter</a> followers
              </div>
              <div class="social">
                <span>325</span>
                <a href="#">RSS feed</a> subscribers
              </div>
            </div>
            <div class="sub-box">
              <h4 class="title-box">Iphone</h4>
              <ul>
                <li><a href="#">IPod</a></li>
                <li><a href="#">IPad</a></li>
                <li><a href="#">ICloud</a></li>
                <li><a href="#">ITune</a></li>
              </ul>
			</div>
            <div class="balon-kata bottom"><i>Apple in-Ear Hendphone whit Remote and Mic</i></div>
            <h4 id="wgs">@wgs</h4>
          </div>  			 
        </div>
	    <div class="clear"></div>  
      <div id="footer">
        <div class="box">
          <div class="sub-box">
            <h4 class="title-box">About Site</h4>
            <p>'one' (compare 'on', in Saxon dialects) and survi Scots as the number 'ane'. Both 'on' (respelled 'one' by the Normans) and 'an' survived into Modern English, with 'one' used as the number and 'an' ('a', before nouns that begin with a consonant sound) as an indefinite article.</p>
          </div>
        </div>
          <div class="box"> 
            <div class="sub-box">
              <h4 class="title-box">Recent Comments</h4>
              <div class="recent-comment">
                <a href="#">'one' (compare 'on', in Saxon dialects) and survi Scots as the number.....</a>
                <span><b>Comment Author</b>, 10-03-2013</span>
              </div>
              <div class="recent-comment">
                <a href="#">'one' (compare 'on', in Saxon dialects) and survi Scots as the number.....</a>
                <span><b>Comment Author</b>, 10-03-2013</span>
              </div>
              <div class="recent-comment">
                <a href="#">'one' (compare 'on', in Saxon dialects) and survi Scots as the number.....</a>
                <span><b>Comment Author</b>, 10-03-2013</span>
              </div>
              <div class="recent-comment">
                <a href="#">'one' (compare 'on', in Saxon dialects) and survi Scots as the number.....</a>
                <span><b>Comment Author</b>, 10-03-2013</span>
              </div>
            </div>  
          </div>
          <div class="box">
            <div class="sub-box">
              <h4 class="title-box">Popular Post</h4>
              <ul>
                <li><a href="#">Marcury fre Baklit dispkay</a></li>
                <li><a href="#">PT.Walden Global Service</a></li>
                <li><a href="#">survived into Modern English</a></li>
                <li><a href="#">PointBlank</a></li>
              </ul>
            </div>  
          </div>
        <div class="clear"></div>
        <h5>Copy @ 2013 <b>PT.Walden Global Service</b> All Right Reserverd</h5>
        </div>
      </div>
    </div>
  </div>           
</body>
</html>
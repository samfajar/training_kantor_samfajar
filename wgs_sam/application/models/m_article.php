<?php

class m_article extends CI_Model {

	function add()
	{	
		$data = array(
			'judul' => $this->input->post('judul'),
			'isi' => $this->input->post('isi'),
		);
		
		$insert = $this->db->insert('article', $data);
		return $insert;
	}
	
		function view($num, $offset)
	{
		$query=$this->db->get("article", $num, $offset); 
		Return $query->result(); 
	}

  public function per_id($id_article) 
  { 
	$this->db->where('idarticle',$id_article); 
	$query=$this->db->get('article'); 
	return $query->result(); 
  }


	public function edit() 
  { 
	$id_article=$this->input->post('id'); 
	$data_article=array( 
	  'judul' => $this->input->post('judul'),			
	  'isi' => $this->input->post('isi')			
	); 
	$this->db->where('idarticle',$id_article); 
	$update=$this->db->update('article',$data_article); 
	return $update;  
  }	

  public function delete($id_article) 
  { 
	$this->db->where('idarticle',$id_article); 
	$hapus=$this->db->delete('article'); 
	return $hapus; 
  }

	
}
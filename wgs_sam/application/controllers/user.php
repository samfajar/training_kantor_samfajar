<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class user extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('m_user');	
	}

	function add()
	{
		$this->load->library('form_validation');
		
		// field name, error message, validation rules
		$this->form_validation->set_rules('name', 'Full Name', 'trim|required');
		$this->form_validation->set_rules('username', 'Username', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim|required');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');

		if(($this->form_validation->run() == FALSE))
		{
				$this->load->view('register');
		}
		
		else
		{			
			if($query = $this->m_user->add())
			{
				redirect('home');
			}
			else
			{
				$this->load->view('register');
			}
		}
		
	}
	
	function login()
	{		
		$query = $this->m_user->login();
		if($query) // if the user's credentials validated...
		{
			$data = array(
				'username' => $this->input->post('username'),
				'status_login' => true
			);
			$this->session->set_userdata($data);
			redirect('');
		}
		else 
		{
			redirect('');
		}
	}
	
	function logout()
	{
		$this->session->sess_destroy();
		redirect('');	
	
	}	
	

	
	
}

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class article extends CI_Controller {

  function __construct()
  {
    parent::__construct();
	$this->load->model('m_article');	
  }

    function add()
	{
      $status_login = $this->session->userdata('status_login');
	  if(!isset($status_login) || $status_login != true)
	  {
			$this->load->view('login');
	  } 
	  else
	  {
        $this->load->library('form_validation');
		$this->form_validation->set_rules('judul', 'tittle', 'trim|required');
		$this->form_validation->set_rules('isi', 'content', 'trim|required');
		if(($this->form_validation->run() == FALSE))
		{
		 $this->load->view('add_article');
		}
		else
		{			
		  if($query = $this->m_article->add())
		  {
		    $this->daftar();
		  }
		  else
		  {
		    $this->load->view('add_article');
		  }
		}	
	  }
	}
	
	
	
	public function daftar($id=NULL)
	{
	  $status_login = $this->session->userdata('status_login');
	  if(!isset($status_login) || $status_login != true)
	  {
	    $this->load->view('login');
	  } 
	  else
	  {
	    $jml = $this->db->get('article');
		$config['base_url'] = base_url().'index.php/article/daftar';
		$config['total_rows'] = $jml->num_rows();
		$config['per_page'] = '3';
		$config['first_page'] = 'Prev';
		$config['last_page'] = 'Next';
		$config['next_page'] = '&laquo;';
		$config['prev_page'] = '&raquo;';
		$this->pagination->initialize($config);
		$data['halaman'] = $this->pagination->create_links();
		$data['data']= $this->m_article->view($config['per_page'], $id);	
		$this->load->view('list_article',$data);		
	  }
	}
	
	public function edit()
	{
	  $status_login = $this->session->userdata('status_login');
	  if(!isset($status_login) || $status_login != true)
	  {
	    $this->load->view('login');
	  }
	  else
	  {
	    $this->load->library('form_validation');
		// field name, error message, validation rules
		$this->form_validation->set_rules('judul', 'Title article', 'trim|required');
		$this->form_validation->set_rules('isi', 'Content article', 'trim|required');
		if(($this->form_validation->run() == FALSE))
		{
		  $id_article=$this->uri->segment(3); 
		  $data['data']=$this->m_article->per_id($id_article);
		  $this->load->view('edit_article',$data);
		}
		else
		{			
		  if($query = $this->m_article->edit())
		  {
	        $this->daftar();
		  }
		  else
		  {
			$id_article=$this->uri->segment(3); 
			$data['data']=$this->m_article->per_id($id_article);
			$this->load->view('edit_article',$data);
		  }
		}
	  }
	}
  
	public function delete()
	{
	  $status_login = $this->session->userdata('status_login');
      if(!isset($status_login) || $status_login != true)
	  {
	    $this->load->view('login');
	  }
	  else
	  {
		$id_article=$this->uri->segment(3); 
		$this->m_article->delete($id_article); 
		$message = "Article was deleted";
		echo "<script>javascript:alert('".$message."'); window.location = '../../../index.php/article/daftar'</script>";
	  }

	}
}
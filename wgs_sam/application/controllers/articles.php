<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Articles extends CI_Controller {

  function __construct()
  {
    parent::__construct();
	$this->load->model('Model_article');	
  }
  
  function add(){
      $status_login = $this->session->userdata('status_login');
	  if($this->validate_login()){
            $this->load->library('form_validation');
		$this->form_validation->set_rules('judul', 'tittle', 'trim|required');
		$this->form_validation->set_rules('isi', 'content', 'trim|required');
		if(($this->form_validation->run() == FALSE))
		{
		 $this->load->view('Articles/add_article');
		}
		else
		{			
		  if($query = $this->Model_article->add())
		  {
		    $this->daftar();
		  }
		  else
		  {
		    $this->load->view('Articles/add_article');
		  }
		}
	  } 
	  else
	  {
              $this->load->view('User/login');	
	  }
	}
	
	
	public function daftar($id=NULL){
	  if($this->validate_login()){
	   $jml = $this->db->get('article');
		$config['base_url'] = base_url().'index.php/Articles/daftar';
		$config['total_rows'] = $jml->num_rows();
		$config['per_page'] = '3';
		$config['first_page'] = 'Prev';
		$config['last_page'] = 'Next';
		$config['next_page'] = '&laquo;';
		$config['prev_page'] = '&raquo;';
		$this->pagination->initialize($config);
		$data['halaman'] = $this->pagination->create_links();
		$data['data']= $this->Model_article->view($config['per_page'], $id);	
		$this->load->view('Articles/list_article',$data);
	  } 
	  else
	  {
              $this->load->view('User/login');	
	  }
	}
	
	public function edit(){
	  if($this->validate_login()){
	    $this->load->library('form_validation');
		// field name, error message, validation rules
		$this->form_validation->set_rules('judul', 'Title article', 'trim|required');
		$this->form_validation->set_rules('isi', 'Content article', 'trim|required');
		if(($this->form_validation->run() == FALSE))
		{
		  $id_article=$this->uri->segment(3); 
		  $data['data']=$this->Model_article->per_id($id_article);
		  $this->load->view('Articles/edit_article',$data);
		}
		else{			
		  if($query = $this->Model_article->edit()){
                    $this->daftar();
		  }
		  else{
			$id_article=$this->uri->segment(3); 
			$data['data']=$this->Model_article->per_id($id_article);
			$this->load->view('Articles/edit_article',$data);
		  }
		}
	  }
	  else{
              $this->load->view('User/login');
	  }
	}
  
	public function delete(){
        if($this->validate_login()){
          $id_article=$this->uri->segment(3); 
          $this->Model_article->delete($id_article); 
          $message = "Article was deleted";
          echo "<script>javascript:alert('".$message."'); window.location = '../../../index.php/Articles/daftar'</script>";$this->load->view('Home');
	}
	  else{
            $this->load->view('User/login');	
	  }
	}
        
        private function validate_login(){
         $status_login = $this->session->userdata('status_login');
            if(!isset($status_login) || $status_login != true)
	  {
              return false;
	  }
          else{
              return true;
          }
        }
}


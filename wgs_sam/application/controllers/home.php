<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	public function index()
	{
	  $status_login = $this->session->userdata('status_login');
	  if(!isset($status_login) || $status_login != true)
	  {
        $this->load->view('login');
      } 
      else
      {
	    $this->load->view('home');		
	  }
	}
	
	public function register()
	{
	  $status_login = $this->session->userdata('status_login');
	  if(!isset($status_login) || $status_login != true)
	  {
	    $this->load->view('register');
	  } 
	  else
	  {
		$this->load->view('home');		
	  }
	}
}

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('Model_user');	
	}
        

        public function index(){
            if($this->validate_login()){
                $this->load->view('Home');
            }
            else{
                $this->load->view('User/login');
            }
        }

	function add()
	{
		$this->load->library('form_validation');
		
		// field name, error message, validation rules
		$this->form_validation->set_rules('name', 'Full Name', 'trim|required');
		$this->form_validation->set_rules('username', 'Username', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim|required');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');

		if(($this->form_validation->run() == FALSE))
		{
				$this->load->view('register');
		}
		
		else
		{			
			if($query = $this->Model_user->add())
			{
				redirect('Users');
			}
			else
			{
				$this->load->view('Users/register');
			}
		}
		
	}
	
	function login()
	{		
		$query = $this->Model_user->login();
		if($query) // if the user's credentials validated...
		{
			$data = array(
				'username' => $this->input->post('username'),
				'password' => $this->input->post('password'),
				'status_login' => true
			);
			$this->session->set_userdata($data);
			redirect('');
		}
		else 
		{
			redirect('');
		}
	}
	
	function logout()
	{
		$this->session->sess_destroy();
		redirect('');	
	
	}
        
        private function validate_login(){
         $status_login = $this->session->userdata('status_login');
            if(!isset($status_login) || $status_login != true)
	  {
              return false;
	  }
          else{
              return true;
          }
        }
        
        public function register(){
          if($this->validate_login()){
            $this->load->view('Articles/home');		
          }
          else {
            $this->load->helper('captcha'); 
            $vals = array(
           'img_path'	 => './captcha/',
           'img_url'	 => base_url().'captcha/',
           'img_width'	 => '200',
           'img_height' => 30,
           'border' => 0, 
           'expiration' => 7200
          );
         // create captcha image
          $cap = create_captcha($vals);
         // store image html code in a variable
         $data['image'] = $cap['image'];
         // store the captcha word in a session
         $this->session->set_userdata('captcha', $cap['word']);
	    if(($this->input->post('token') != $this->session->userdata('captcha')) && $this->input->post()){
	 	$data['captcha'] = "Captcha invalid !";
	    } 
            else {
	  	$data['captcha'] = "";
	    }
            $this->load->view('User/register', $data);
   }
  }
	

	
	
}

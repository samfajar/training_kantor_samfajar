<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// Created on Aug 26, 2011 by Damiano Venturin @ Squadra Informatica

class Articles extends CI_Controller {
	public function __construct() 
	{
		parent::__construct();
		$this->load->config('rest');
		$this->load->spark('restclient/2.0.0');		
		$this->rest->initialize(array(  
    	  'server' => 'http://127.0.0.1/wgs_sam/',  
    	  'http_user' => 'admin',  
    	  'http_pass' => '1234',  
    	  'http_auth' => 'digest' // or 'digest'  
		));  
	}
	
	public function index()
	{
		$this->getArticles();
	}

	public function getArticles()
	{
		echo '<pre>';
		print_r($this->rest->get('api/api_articles/articles'));
		echo '</pre>';
	}
	
}